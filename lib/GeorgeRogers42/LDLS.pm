package GeorgeRogers42::LDLS;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('index#pages');
  $r->get('/pages/')->to('index#pages');
  $r->get('/page/:slug')->to('index#page');
}

1;
