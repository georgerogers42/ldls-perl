package GeorgeRogers42::LDLS::Pages;
use Moo;
use JSON;
use GeorgeRogers42::LDLS::Page;
use namespace::clean;

has path => (is => 'ro');
has pages => (is => 'lazy');
has page_map => (is => 'lazy');

sub _build_pages {
  my $self = shift;
  my $path = $self->path;
  my $pages = [];
  for my $fname (<$path/*.markdown>) {
    open my $file, "<", $fname;
    my $mdat = "";
    while((my $line = <$file>) ne "\n") {
      $mdat .= $line;
    }
    my $mdata = JSON::decode_json($mdat);
    my $contents = join "", <$file>;
    my $page = GeorgeRogers42::LDLS::Page->new(%$mdata, contents => $contents);
    push @$pages, $page;
    close $file;
  }
  $pages = [sort { $a->sequence cmp $b->sequence } @$pages];
  return $pages;
}

sub _build_page_map {
  my $self = shift;
  my $page_map = {};
  my $pages = $self->pages;
  for my $page (@$pages) {
    $page_map->{$page->slug} = $page;
  }
  return $page_map;
}

sub page {
  my ($self, $str) = @_;
  $self->page_map->{$str};
}

my $instance;
sub instance {
  $instance //= __PACKAGE__->new(path => "articles");
}

1;
