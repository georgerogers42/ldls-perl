package GeorgeRogers42::LDLS::Controller::Index;
use Mojo::Base 'Mojolicious::Controller';
use GeorgeRogers42::LDLS::Pages;

my $pages = GeorgeRogers42::LDLS::Pages->instance;
# This action will render a template
sub pages {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(pages => $pages->pages);
}

sub page {
  my $self = shift;
  my $slug = $self->stash('slug');
  $self->render(page => $pages->page($slug));
}

1;
