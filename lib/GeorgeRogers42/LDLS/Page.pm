package GeorgeRogers42::LDLS::Page;
use Moo;
use Text::Markdown qw(markdown);
use DateTime::Format::Pg;
use namespace::clean;

has posted => (is => 'lazy');
has posted_on => (is => 'ro');
has slug => (is => 'ro');
has title => (is => 'ro');
has contents => (is => 'ro');
has rendered => (is => 'lazy');

sub _build_posted {
  my $self = shift;
  my $time = DateTime::Format::Pg->parse_timestamptz($self->posted_on);
  $time->set_formatter('DateTime::Format::Pg');
  return $time;
}

sub _build_rendered {
  my $self = shift;
  markdown $self->contents;
};

1;
