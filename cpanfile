requires 'Mojolicious';
requires 'JSON';
requires 'Moo';
requires 'namespace::clean';
requires 'Text::Markdown';
requires 'Starman';
requires 'DateTime::Format::Pg';
requires 'DateTime';
